package kg.microgram.microgram.controller;

import kg.microgram.microgram.annotations.ApiPageable;
import kg.microgram.microgram.dto.CommentDTO;
import kg.microgram.microgram.dto.PublicationDTO;
import kg.microgram.microgram.dto.ResponceDTO;
import kg.microgram.microgram.service.CommentService;
import kg.microgram.microgram.service.PublicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/comments")
public class CommentController {
    @Autowired
    CommentService commentService;

    /*@ApiPageable
    @GetMapping
    public Slice<CommentDTO> findComments(@ApiIgnore Pageable pageable) {
        return commentService.findComments(pageable);
    }*/
    @GetMapping
    public List<CommentDTO> findCommentsList() {
        return commentService.findCommentsList();
    }
    @GetMapping("/{id}")
    public CommentDTO getComment(@PathVariable String id) {
        return commentService.findOne(id);
    }
    //Удаление комментария
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteComment(Authentication authentication, @PathVariable String id) {
        if (commentService.deleteComment(authentication, id))
            return ResponseEntity.noContent().build();
        return ResponseEntity.notFound().build();
    }
    //Добавление комментария
    /*@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponceDTO addComment(@RequestBody CommentDTO commentData, Authentication authentication) {
        return commentService.addComment(commentData, authentication);
    }*/
    @PostMapping(name = "/add")
    public void addComment(@RequestParam("publicationId") String publicationId,
                                  @RequestParam("text") String text,
                                  @RequestParam("userId") String userId,
                                  HttpServletResponse response) throws IOException {
        commentService.addNewComment(publicationId, text, userId);
        response.sendRedirect("/");
        //return "redirect:/publications";
    }


}
