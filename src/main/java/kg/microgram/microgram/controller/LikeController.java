package kg.microgram.microgram.controller;

import kg.microgram.microgram.annotations.ApiPageable;
import kg.microgram.microgram.dto.LikeDTO;
import kg.microgram.microgram.dto.ResponceDTO;
import kg.microgram.microgram.dto.SubscribeDTO;
import kg.microgram.microgram.service.LikeService;
import kg.microgram.microgram.service.SubscribeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/likes")
public class LikeController {
    @Autowired
    LikeService likeService;

    @ApiPageable
    @GetMapping
    public Slice<LikeDTO> findLikes(@ApiIgnore Pageable pageable) {
        return likeService.findLikes(pageable);
    }
    @GetMapping("/{id}")
    public LikeDTO getLike(@PathVariable String id) {
        return likeService.findOne(id);
    }

    //Удаление лайка
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteLike(@PathVariable String id) {
        if (likeService.deleteLike(id))
            return ResponseEntity.noContent().build();

        return ResponseEntity.notFound().build();
    }
    //Добавление лайка
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponceDTO like(@RequestBody LikeDTO likeData, Authentication authentication) {
        return likeService.addLike(likeData, authentication);
    }



}
