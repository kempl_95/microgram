package kg.microgram.microgram.controller;

import kg.microgram.microgram.annotations.ApiPageable;
import kg.microgram.microgram.dto.NewPublicationDTO;
import kg.microgram.microgram.dto.PublicationDTO;
import kg.microgram.microgram.dto.ResponceDTO;
import kg.microgram.microgram.dto.UserDTO;
import kg.microgram.microgram.service.CommentService;
import kg.microgram.microgram.service.PublicationImageService;
import kg.microgram.microgram.service.PublicationService;
import kg.microgram.microgram.service.UserService;
import org.springframework.security.core.Authentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/publications")
public class PublicationController {
    @Autowired
    PublicationService publicationService;
    @Autowired
    CommentService commentService;

    /*@ApiPageable
    @GetMapping
    public Slice<PublicationDTO> findPublications(@ApiIgnore Pageable pageable) {
        return publicationService.findPublications(pageable);
    }*/
    @GetMapping
    @ResponseBody
    public List<PublicationDTO> getUserPublicationByAuthorizedUser(Authentication authentication) {
        return publicationService.findUserPublicationsByID(authentication);
    }
    @GetMapping("/{id}")
    public PublicationDTO getPublication(@PathVariable String id) {
        return publicationService.findOne(id);
    }
    //Удаление публикации.
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Void> deletePublication(Authentication authentication, @PathVariable String id) {
        if (publicationService.deletePublication(authentication, id))
            return ResponseEntity.noContent().build();

        return ResponseEntity.notFound().build();
    }
    //Просмотр лент других пользователей. Мы можем просмотреть публикации, которые сделал этот пользователь.
    @GetMapping("/all")
    public List<PublicationDTO> findAllPublications() {
        //publicationService.findPublicationList().forEach(System.out::println);
        return publicationService.findPublicationList();
    }
    //Добавление публикации
    /*@PostMapping(name = "/add",consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponceDTO addPublication(@RequestBody PublicationDTO publicationData, Authentication authentication) {
        return publicationService.addPublication(publicationData, authentication);
    }*/
    @PostMapping(name = "/add")
    @ResponseBody
    public void addPublication2(@RequestParam("file") MultipartFile file,
                                  @RequestParam("description") String description,
                                  @RequestParam("userId") String userId,
                                  HttpServletResponse response) throws IOException {
        //System.out.println("TEST!!!");
        publicationService.addNewPublication(file, description, userId);
        response.sendRedirect("/");
        //return "success";
    }
    // 4) Пользователь может отметить те публикации которые ему понравились.
    @GetMapping("/liked/{userId}")
    public List<PublicationDTO> getLikedPublications(@PathVariable String userId) {
        return publicationService.findLikedPublications(userId);
    }


}
