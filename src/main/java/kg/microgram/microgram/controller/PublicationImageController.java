package kg.microgram.microgram.controller;

import kg.microgram.microgram.dto.PublicationImageDTO;
import kg.microgram.microgram.model.Publication;
import kg.microgram.microgram.service.PublicationImageService;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

@RestController
@RequestMapping("/img")
public class PublicationImageController {

    private final PublicationImageService publicationImageService;

    public PublicationImageController(PublicationImageService publicationImageService) {
        this.publicationImageService = publicationImageService;
    }

    @PostMapping
    public PublicationImageDTO addPublicationImage(@RequestParam("file") MultipartFile file) {
        return publicationImageService.addImage(file);
    }
    @GetMapping("/{name}")
    public ResponseEntity<byte[]> serveFile(@PathVariable String name) {
        String path = "C:/Work/JAVA/Different/img";
        try{
            //InputStream is = new ClassPathResource(name).getInputStream();
            //ClassLoader classLoader = getClass().getClassLoader();
            //InputStream is = classLoader.getResourceAsStream(name);
            //File file = new File(classLoader.getResource("fileTest.txt").getFile());
            InputStream is = new FileInputStream(new File(path) + "/" + name);
            return ResponseEntity.ok()
                    //.contentType(name.toLowerCase().contains(".png")?MediaType.IMAGE_PNG:MediaType.IMAGE_JPEG)
                    .header(HttpHeaders.CONTENT_TYPE, MediaType.IMAGE_JPEG_VALUE)
                    .body(StreamUtils.copyToByteArray(is));
        }catch (Exception e){
            InputStream is = null;
            try {
                is = new FileInputStream(new File(path) + "/" + name);
                return ResponseEntity.ok()
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.IMAGE_JPEG_VALUE)
                        .body(StreamUtils.copyToByteArray(is));
            }catch (Exception ex){
                ex.printStackTrace();
            }
            e.printStackTrace();
        }
        return null;
    }
}
