package kg.microgram.microgram.controller;

import kg.microgram.microgram.annotations.ApiPageable;
import kg.microgram.microgram.dto.PublicationDTO;
import kg.microgram.microgram.dto.ResponceDTO;
import kg.microgram.microgram.dto.SubscribeDTO;
import kg.microgram.microgram.model.Subscribe;
import kg.microgram.microgram.service.PublicationService;
import kg.microgram.microgram.service.SubscribeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

@RestController
@RequestMapping("/subscribes")
public class SubscribeController {
    @Autowired
    SubscribeService subscribeService;

    @ApiPageable
    @GetMapping
    public Slice<SubscribeDTO> findSubscribes(@ApiIgnore Pageable pageable) {
        return subscribeService.findSubscribes(pageable);
    }
    @GetMapping("/{id}")
    public SubscribeDTO getSubscribe(@PathVariable String id) {
        return subscribeService.findOne(id);
    }

    //Удаление подписки
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteSubscribe(@PathVariable String id) {
        if (subscribeService.deleteSubscribe(id))
            return ResponseEntity.noContent().build();

        return ResponseEntity.notFound().build();
    }
    //Добавление подписки
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponceDTO subscribe(@RequestBody SubscribeDTO subscribeData, Authentication authentication) {
        return subscribeService.addSubscribe(subscribeData, authentication);
    }



}
