package kg.microgram.microgram.controller;

import kg.microgram.microgram.annotations.ApiPageable;
import kg.microgram.microgram.dto.CommentDTO;
import kg.microgram.microgram.dto.NewUserDTO;
import kg.microgram.microgram.dto.ResponceDTO;
import kg.microgram.microgram.dto.UserDTO;
import kg.microgram.microgram.service.PublicationService;
import kg.microgram.microgram.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@RequestMapping("/users")
public class UserController {
    @Autowired
    UserService userService;

    @ApiPageable
    @GetMapping
    public Slice<UserDTO> findUsers(@ApiIgnore Pageable pageable) {
        return userService.findUsers(pageable);
    }
    @GetMapping("/{userId}")
    public UserDTO getUser(@PathVariable String userId) {
        return userService.findOne(userId);
    }
    @GetMapping("/name/{name}")
    public UserDTO getUserByName(@PathVariable String name) {
        return userService.findOneByName(name);
    }
    @GetMapping("/login/{login}")
    public UserDTO getUserByLogin(@PathVariable String login) {
        return userService.findOneByLogin(login);
    }
    @GetMapping("/email/{email}")
    public UserDTO getUserByEmail(@PathVariable String email) {
        return userService.findOneByEmail(email);
    }
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponceDTO addUser(@RequestBody NewUserDTO userData) {
        return userService.addUser(userData);
    }
    /*@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponceDTO addUser(@RequestParam("email") String email,
                               @RequestParam("login") String login,
                               @RequestParam("password") String password,
                               @RequestParam("name") String name,
                               @RequestParam("surname") String surname) {
        return userService.addUser2(email, login, password, name, surname);
    }*/
    /*@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void addUser(@RequestBody NewUserDTO userData,
                               HttpServletResponse response) {
         userService.addUser(userData);
         response.encodeRedirectURL("/");
    }*/

}
