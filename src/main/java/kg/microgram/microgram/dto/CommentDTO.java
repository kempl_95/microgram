package kg.microgram.microgram.dto;

import kg.microgram.microgram.model.Comment;
import kg.microgram.microgram.model.Publication;
import kg.microgram.microgram.model.User;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class CommentDTO {

    public static CommentDTO from(Comment comment) {
        return builder()
                .id(comment.getId())
                .text(comment.getText())
                .datetime(comment.getDatetime())
                .publication(comment.getPublication())
                .user(comment.getUser())
                .build();
    }
    public static List<CommentDTO> fromList(List<Comment> commentList) {
        List<CommentDTO> cList = new ArrayList<>();
        commentList.stream().forEach(publication -> {
            cList.add(CommentDTO.from(publication));
        });
        return cList;
    }
    private String id;
    private String text;
    private String datetime;
    private Publication publication;    //к какой публикации
    private User user;

}
