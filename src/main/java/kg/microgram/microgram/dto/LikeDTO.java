package kg.microgram.microgram.dto;

import kg.microgram.microgram.model.Like;
import kg.microgram.microgram.model.Publication;
import kg.microgram.microgram.model.User;
import lombok.*;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class LikeDTO {

    public static LikeDTO from(Like like) {
        return builder()
                .id(like.getId())
                .user(like.getUser())
                .publication(like.getPublication())
                .datetime(like.getDatetime())
                .build();
    }

    public static List<LikeDTO> fromList(List<Like> likeList) {
        List<LikeDTO> lList = new ArrayList<>();
        likeList.stream().forEach(like -> {
            lList.add(LikeDTO.from(like));
        });
        return lList;
    }

    private String id;
    private User user;
    private Publication publication;
    private String datetime;

}
