package kg.microgram.microgram.dto;

import kg.microgram.microgram.model.Publication;
import kg.microgram.microgram.model.PublicationImage;
import kg.microgram.microgram.model.User;
import lombok.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class NewPublicationDTO {

    public static NewPublicationDTO from(PublicationImage img, String description, String userId) {
        return builder()
                .img(img)
                .description(description)
                .userId(userId)
                .build();
    }
    private PublicationImage img;
    private String description;
    private String userId;             //кто публиковал
}
