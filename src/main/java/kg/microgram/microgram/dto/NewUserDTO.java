package kg.microgram.microgram.dto;

import kg.microgram.microgram.model.User;
import lombok.*;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class NewUserDTO {

    public static NewUserDTO from(User user) {
        return builder()
                .name(user.getName())
                .surname(user.getSurname())
                .login(user.getLogin())
                .email(user.getEmail())
                .password(user.getPassword())
                .build();
    }

    private String name;
    private String surname;
    private String login;
    private String email;
    private String password;
}
