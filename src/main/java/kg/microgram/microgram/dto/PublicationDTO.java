package kg.microgram.microgram.dto;

import kg.microgram.microgram.model.*;
import kg.microgram.microgram.repository.Comments;
import lombok.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class PublicationDTO {

    public static PublicationDTO from(Publication publication) {
        return builder()
                .id(publication.getId())
                .image(publication.getImage())
                .description(publication.getDescription())
                .datetime(publication.getDatetime())
                .user(publication.getUser())
                .build();
    }
    public static PublicationDTO fromWithComments(Publication publication, List<Comment> commentsList) {
        return builder()
                .id(publication.getId())
                .image(publication.getImage())
                .description(publication.getDescription())
                .datetime(publication.getDatetime())
                .user(publication.getUser())
                .commentsList(commentsList)
                .build();
    }

    private String id;
    private String image;
    private String description;
    private String datetime;
    private User user;             //кто публиковал
    private List<Comment> commentsList;

}
