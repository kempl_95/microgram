package kg.microgram.microgram.dto;

import kg.microgram.microgram.model.User;
import lombok.*;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class ResponceDTO {
    private String id;
    private String answer;

    public static ResponceDTO from(String id, String answer) {
        return builder()
                .id(id)
                .answer(answer)
                .build();
    }
}
