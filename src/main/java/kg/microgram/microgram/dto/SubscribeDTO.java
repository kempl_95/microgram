package kg.microgram.microgram.dto;

import kg.microgram.microgram.model.Publication;
import kg.microgram.microgram.model.Subscribe;
import kg.microgram.microgram.model.User;
import lombok.*;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class SubscribeDTO {

    public static SubscribeDTO from(Subscribe subscribe) {
        return builder()
                .id(subscribe.getId())
                .userSubscriber(subscribe.getUserSubscriber())
                .userSubscribedTo(subscribe.getUserSubscribedTo())
                .build();
    }

    public static List<SubscribeDTO> fromList(List<Subscribe> subscribeList) {
        List<SubscribeDTO> sList = new ArrayList<>();
        subscribeList.stream().forEach(subscribe -> {
                    sList.add(SubscribeDTO.from(subscribe));
                });
        return sList;
    }

    private String id;
    private User userSubscriber;                //Подписчик
    private User userSubscribedTo;

}
