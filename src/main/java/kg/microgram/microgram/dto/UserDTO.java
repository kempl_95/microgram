package kg.microgram.microgram.dto;

import kg.microgram.microgram.model.User;
import lombok.*;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class UserDTO {

    public static UserDTO from(User user) {
        return builder()
                .id(user.getId())
                .name(user.getName())
                .surname(user.getSurname())
                .login(user.getLogin())
                .email(user.getEmail())
                .password(user.getPassword())
                .publicationQty(user.getPublicationQty())
                .subscriptionQty(user.getSubscriptionQty())
                .subscriberQty(user.getSubscriberQty())
                .build();
    }

    private String id;
    private String name;
    private String surname;
    private String login;
    private String email;
    private String password;
    private int publicationQty;
    private int subscriptionQty;            //подписки
    private int subscriberQty;

}
