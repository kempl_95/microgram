package kg.microgram.microgram.model;

import kg.microgram.microgram.utils.Generator;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.Random;
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
@Document(collection = "comment")
public class Comment {
    private static final Random rn = new Random();
    public static Comment addBegComments(String id, User user, Publication publication) {
        return builder()
                .id(id)
                .text(Generator.makeDescription())
                .datetime(LocalDateTime.now().toString())
                .user(user)
                .publication(publication)
                .build();
    }
    @Id
    private String id;
    private String text;
    @Indexed
    private String datetime;
    @DBRef
    @Indexed
    private Publication publication;    //к какой публикации
    @DBRef
    @Indexed
    private User user;                  //кто оставил комментарий
}
