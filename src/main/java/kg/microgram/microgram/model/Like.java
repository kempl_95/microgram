package kg.microgram.microgram.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.Random;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
@Document(collection = "like")
public class Like {
    private static final Random rn = new Random();
    public static Like addBegLikes(String id, User user, Publication publication) {
        return builder()
                .id(id)
                .user(user)
                .publication(publication)
                .datetime(LocalDateTime.now().toString())
                .build();
    }
    public static Like addNewLike(Like like) {
        return builder()
                .user(like.getUser())
                .publication(like.getPublication())
                .datetime(LocalDateTime.now().toString())
                .build();
    }
    @Id
    private String id;
    @DBRef
    @Indexed
    private User user;
    @DBRef
    @Indexed
    private Publication publication;
    @Indexed
    private String datetime;
}
