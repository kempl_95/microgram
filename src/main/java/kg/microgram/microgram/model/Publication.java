package kg.microgram.microgram.model;

import kg.microgram.microgram.utils.Generator;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.Random;
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
@Document(collection = "publication")
public class Publication {
    private static final Random rn = new Random();
    public static Publication addBegPublications(String id, User user) {
        return builder()
                .id(id)
                .image("no_img.jpeg")
                .description(Generator.makeDescription())
                .datetime(LocalDateTime.now().toString())
                .user(user)
                .commentsQty(rn.nextInt(5)+1)
                .build();
    }
    @Id
    private String id;
    private String image;
    private String description;
    @Indexed
    private String datetime;
    @DBRef
    @Indexed
    private User user;             //кто публиковал
    private int commentsQty;
}
