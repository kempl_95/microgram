package kg.microgram.microgram.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import java.util.Random;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
@Document(collection = "subscribe")
public class Subscribe {
    private static final Random rn = new Random();
    public static Subscribe addBegSubscribes(String id, User userSubscriber, User userSubscribedTo) {
        return builder()
                .id(id)
                .userSubscriber(userSubscriber)
                .userSubscribedTo(userSubscribedTo)
                .build();
    }
    @Id
    private String id;
    @DBRef
    @Indexed
    private User userSubscriber;                //Подписчик
    @DBRef
    @Indexed
    private User userSubscribedTo;              //на кого подписался
}
