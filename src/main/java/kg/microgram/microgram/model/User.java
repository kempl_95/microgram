package kg.microgram.microgram.model;

import kg.microgram.microgram.utils.Generator;
import kg.microgram.microgram.utils.data.GenerateData;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Collection;
import java.util.List;
import java.util.Random;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
@Document(collection = "user")
public class User implements UserDetails {
    private static final Random rn = new Random();
    public static User addBegUsers(String id, String name, String surname, String login) {
        return builder()
                .id(id)
                .name(name)
                .surname(surname)
                .login(login)
                .email(login.concat("@gmail.com"))
                .password(new BCryptPasswordEncoder().encode(id+id+id))
                .publicationQty(rn.nextInt(5)+1)
                .subscriptionQty(rn.nextInt(5)+1)
                .subscriberQty(rn.nextInt(5)+1)
                .build();
    }
    public static User admin() {
        return builder()
                .email("admin@gmail.com")
                .name("Admin")
                .password(new BCryptPasswordEncoder().encode("admin"))
                .build();
    }
    public static User guest() {
        return builder()
                .email("guest@gmail.com")
                .name("Guest")
                .password(new BCryptPasswordEncoder().encode("guest"))
                .build();
    }

    public static final User EMPTY = new User();

    @Id
    private String id;
    @Indexed
    private String name;
    @Indexed
    private String surname;
    @Indexed
    private String login;
    @Indexed
    private String email;
    @Indexed
    private String password;
    private int publicationQty;
    private int subscriptionQty;            //подписки
    private int subscriberQty;              //подписщики
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(new SimpleGrantedAuthority("FULL"));
    }
    @Override
    public String getUsername() {
        return getLogin();
    }
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }
    @Override
    public boolean isEnabled() {
        return true;
    }
}
