package kg.microgram.microgram.repository;

import kg.microgram.microgram.model.Comment;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface Comments extends PagingAndSortingRepository<Comment, String> {
    //Поиск по id комментария для удаления. И только если id авторизованного пользователя совпадает с id пользователя, оставившего комментарий
    //
    public void deleteAllByPublicationId(String publicationId);
    public boolean existsAllByPublicationId(String publicationId);
    public List<Comment> findAllByPublicationId(String publicationId);
    public List<Comment> findAll();
}
