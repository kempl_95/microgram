package kg.microgram.microgram.repository;

import kg.microgram.microgram.model.Like;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface Likes extends PagingAndSortingRepository<Like, String> {
    //Если публикация имеет лайк, то можно оставить комментарий
    //Проверка, есть ли лайк на id публикации под id авторизованного пользователя
    public List<Like> findAllByUserId(String userId);
}
