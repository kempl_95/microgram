package kg.microgram.microgram.repository;

import kg.microgram.microgram.model.PublicationImage;
import org.springframework.data.repository.CrudRepository;

public interface PublicationImages extends CrudRepository<PublicationImage, String> {
    public PublicationImage getById(String id);
}
