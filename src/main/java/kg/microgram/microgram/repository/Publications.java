package kg.microgram.microgram.repository;

import kg.microgram.microgram.model.Publication;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface Publications extends PagingAndSortingRepository<Publication, String> {
    public Publication findFirstById(String id);
    public List<Publication> findAll();
    public Publication getById(String id);
    //Публикации пользователя по ID пользователя
    public List<Publication> findAllByUserId(String publisherId);
    public List<Publication> findAllByUser_Id(String publisherId);
    public List<Publication> findByUserId(String publisherId);
    public List<Publication> getByUserId(String publisherId);
    //Публикации пользователя по Email пользователя
    public List<Publication> findAllByUserEmail(String publisherEmail);
    //Публикации пользователя по Login пользователя
    public List<Publication> findAllByUserLogin(String publisherLogin);
    //Удаление публикации по id
    public void deletePublicationById(String id);

}
