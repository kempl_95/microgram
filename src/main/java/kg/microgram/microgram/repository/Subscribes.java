package kg.microgram.microgram.repository;

import kg.microgram.microgram.model.Subscribe;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface Subscribes extends PagingAndSortingRepository<Subscribe, String> {
    //проверка, если уже подписан на пользователя, то в списке пользователей указывать "уже подписан" или давать возможность подписаться
    //Список подписок по Id подписщика
        //Почему-то не находит ничего...
    List<Subscribe> getAllByUserSubscriberId(String id);
        //эти тоже не работают
    //public List<Subscribe> findSubscribeByUserSubscriberId(int id);
    //public List<Subscribe> findAllByUserSubscriber_Id(int id);

}
