package kg.microgram.microgram.repository;

import kg.microgram.microgram.model.User;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;

public interface Users extends PagingAndSortingRepository<User, String> {
    //Поиск пользователя по имени/фамилии/email/логину
    public User getById(String id);
    public User findByEmail(String email);
    public User findByName(String name);
    public User findBySurname(String surname);
    public User findByNameAndSurname(String name, String surname);
    public User findByLogin(String login);
    public Optional<User> getByEmail(String s);
    public Optional<User> getByLogin(String s);
    //OLD
    //при регистрации нового пользователя, проверить по email, существует ли такой-же
    public boolean existsByEmail(String email);
    public boolean existsByLogin(String login);
    //при входе (login) проверить email и password на совпадение
    public User getFirstByEmailAndPassword(String email, String password);
    public boolean existsByEmailAndPassword(String email, String password);
    //при входе (login) проверить login и password на совпадение
    public User getFirstByLoginAndPassword(String login, String password);
    public boolean existsByLoginAndPassword(String login, String password);


}
