package kg.microgram.microgram.service;

import kg.microgram.microgram.dto.CommentDTO;
import kg.microgram.microgram.dto.PublicationDTO;
import kg.microgram.microgram.dto.ResponceDTO;
import kg.microgram.microgram.exception.ResourceNotFoundException;
import kg.microgram.microgram.model.Comment;
import kg.microgram.microgram.model.Publication;
import kg.microgram.microgram.model.PublicationImage;
import kg.microgram.microgram.model.User;
import kg.microgram.microgram.repository.Comments;
import kg.microgram.microgram.repository.Likes;
import kg.microgram.microgram.repository.Publications;
import kg.microgram.microgram.repository.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

@Service
public class CommentService {
    @Autowired
    Comments comments;
    @Autowired
    Likes likes;
    @Autowired
    Publications publications;
    @Autowired
    Users users;
    /*
    public Slice<CommentDTO> findComments(Pageable pageable) {
        var slice = comments.findAll(pageable);
        return slice.map(CommentDTO::from);
    }*/
    public List<CommentDTO> findCommentsList() {
        List<CommentDTO> commentDTOList = new ArrayList<>();
        comments.findAll().stream().forEach(obj ->
                commentDTOList.add(CommentDTO.from(obj)));
        return commentDTOList;
    }

    public CommentDTO findOne(String id) {
        var user = comments.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Can't find comment with the ID: " + id));
        return CommentDTO.from(user);
    }
    public ResponceDTO addComment(CommentDTO commentData, Authentication authentication) {
        //Пользователь оставляет комментарии к понравившимся ему публикациям.
        try{
            User user = (User)authentication.getPrincipal();
            var obj = Comment.builder()
                    .text(commentData.getText())
                    .datetime(LocalDateTime.now().toString())
                    .publication(commentData.getPublication())
                    .user(user)
                    .build();
            comments.save(obj);
            return ResponceDTO.from(obj.getId(), "Comment " + user.getId() + " created Successfully");
        } catch (java.lang.IndexOutOfBoundsException ex){
            return ResponceDTO.from("400", "Error!");
        }
    }
    public void addNewComment(String publicationId, String text, String userId) {
        //Пользователь оставляет комментарии к понравившимся ему публикациям.
            var obj = Comment.builder()
                    .text(text)
                    .datetime(LocalDateTime.now().toString())
                    .publication(publications.getById(publicationId))
                    .user(users.getById(userId))
                    .build();
            comments.save(obj);
    }
    public boolean deleteComment(Authentication authentication, String commentId) {
        AtomicBoolean res = new AtomicBoolean(false);
        User user = (User)authentication.getPrincipal();
        comments.findById(commentId).stream().forEach(comment -> {
            if (comment.getPublication().getUser().getId().equals(user.getId())){
                comments.deleteById(commentId);
                res.set(true);
            }
        });
        return res.get();
    }
}
