package kg.microgram.microgram.service;

import kg.microgram.microgram.dto.LikeDTO;
import kg.microgram.microgram.dto.ResponceDTO;
import kg.microgram.microgram.dto.SubscribeDTO;
import kg.microgram.microgram.exception.ResourceNotFoundException;
import kg.microgram.microgram.model.*;
import kg.microgram.microgram.repository.Likes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class LikeService {
    @Autowired
    Likes likes;
    public Slice<LikeDTO> findLikes(Pageable pageable) {
        var slice = likes.findAll(pageable);
        return slice.map(LikeDTO::from);
    }

    public LikeDTO findOne(String id) {
        var obj = likes.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Can't find like with the ID: " + id));
        return LikeDTO.from(obj);
    }
    public ResponceDTO addLike(LikeDTO likeData, Authentication authentication) {
        try{
            User user = (User)authentication.getPrincipal();
            var obj = Like.builder()
                    .user(user)
                    .publication(likeData.getPublication())
                    .datetime(LocalDateTime.now().toString())
                    .build();
            likes.save(obj);
            return ResponceDTO.from(obj.getId(), "Like " + obj.getId() + " created Successfully");
        } catch (java.lang.IndexOutOfBoundsException ex){
            return ResponceDTO.from("400", "Error!");
        }
    }

    public boolean deleteLike(String id) {
        likes.deleteById(id);
        return true;
    }
}
