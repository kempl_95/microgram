package kg.microgram.microgram.service;

import kg.microgram.microgram.dto.PublicationImageDTO;
import kg.microgram.microgram.exception.ResourceNotFoundException;
import kg.microgram.microgram.model.PublicationImage;
import kg.microgram.microgram.repository.PublicationImages;
import org.bson.types.Binary;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
public class PublicationImageService {
    private final PublicationImages publicationImages;

    public PublicationImageService(PublicationImages publicationImages) {
        this.publicationImages = publicationImages;
    }

    public PublicationImageDTO addImage(MultipartFile file) {
        byte[] data = new byte[0];
        try {
            data = file.getBytes();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (data.length == 0) {
            // TODO return no content or something or throw exception
            //  which will be processed on controller layer
        }

        Binary bData = new Binary(data);
        PublicationImage image = PublicationImage.builder().posterData(bData).build();

        publicationImages.save(image);

        return PublicationImageDTO.builder()
                .imageId(image.getId())
                .build();
    }

    public Resource getById(String imageId) {
        PublicationImage publicationImage = publicationImages.findById(imageId)
                .orElseThrow(() -> new ResourceNotFoundException("Publication Image with " + imageId + " doesn't exists!"));
        return new ByteArrayResource(publicationImage.getPosterData().getData());
    }
    /*public Resource getById(String imageId) {
        PublicationImage publicationImage = publicationImages.getById(imageId);
        return new ByteArrayResource(publicationImage.getPosterData().getData());
    }*/
}
