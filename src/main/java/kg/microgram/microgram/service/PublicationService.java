package kg.microgram.microgram.service;

import kg.microgram.microgram.dto.PublicationDTO;
import kg.microgram.microgram.dto.ResponceDTO;
import kg.microgram.microgram.exception.ResourceNotFoundException;
import kg.microgram.microgram.model.*;
import kg.microgram.microgram.repository.*;
import org.bson.types.Binary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class PublicationService {
    @Autowired
    Publications publications;
    @Autowired
    Comments comments;
    @Autowired
    Likes likes;
    @Autowired
    Users users;
    @Autowired
    PublicationImages publicationImages;

    public Slice<PublicationDTO> findPublications(Pageable pageable) {
        var slice = publications.findAll(pageable);
        return slice.map(PublicationDTO::from);
    }
    public List<PublicationDTO> findPublicationList() {
        List<PublicationDTO> publicationDTOList = new ArrayList<>();
        publications.findAll().stream().forEach(obj ->
                publicationDTOList.add(PublicationDTO.fromWithComments(obj, comments.findAllByPublicationId(obj.getId()))));
        return publicationDTOList;
    }
    public List<PublicationDTO> findLikedPublications(String userId) {
        List<PublicationDTO> publicationDTOList = new ArrayList<>();
        likes.findAllByUserId(userId).stream().forEach(like ->
                publicationDTOList.add(PublicationDTO.from(like.getPublication())));

        return publicationDTOList;
    }

    public PublicationDTO findOne(String publicationId) {
        var publication = publications.findById(publicationId)
                .orElseThrow(() -> new ResourceNotFoundException("Can't find publication with the ID: " + publicationId));
        return PublicationDTO.from(publication);
    }

    public List<PublicationDTO> findUserPublicationsByID(Authentication authentication) {
        List<PublicationDTO> publicationDTOList = new ArrayList<>();
        User user = (User)authentication.getPrincipal();
        var publicationList = publications.findByUserId(user.getId());
        publicationList.forEach(p -> {
            publicationDTOList.add(PublicationDTO.fromWithComments(p, comments.findAllByPublicationId(p.getId())));
        });

        return publicationDTOList;
    }

    public ResponceDTO addPublication(PublicationDTO publicationData, Authentication authentication) {
        try{
            User user = (User)authentication.getPrincipal();
            var obj = Publication.builder()
                    .image(publicationData.getImage())
                    .description(publicationData.getDescription())
                    .datetime(LocalDateTime.now().toString())
                    .user(user)
                    .commentsQty(0)
                    .build();
            publications.save(obj);
            return ResponceDTO.from(obj.getId(), "Publication " + obj.getId() + " created Successfully");
        } catch (java.lang.IndexOutOfBoundsException ex){
            return ResponceDTO.from("400", "Error!");
        }
    }

    public void addNewPublication(MultipartFile file, String description, String userId) {
        try {
            //String path = "C:/Work/JAVA/Different/img";
            //InputStream is = new FileInputStream(new File(path) + "/" + name);

            //File imageFile = new File("src/main/resources/static/img/" + Objects.requireNonNull(file.getOriginalFilename()).toLowerCase());
            //File imageFileTarget = new File("target/classes/static/img/" + Objects.requireNonNull(file.getOriginalFilename()).toLowerCase());
            File imageFile = new File("C:/Work/JAVA/Different/img/" + Objects.requireNonNull(file.getOriginalFilename()).toLowerCase());
            FileOutputStream os = new FileOutputStream(imageFile);
            //FileOutputStream os2 = new FileOutputStream(imageFileTarget);
            os.write(file.getBytes());
            //os2.write(file.getBytes());
            os.close();
            //os2.close();
            //Binary bData = new Binary(file.getBytes());
            /*PublicationImage publicationImage = PublicationImage.builder()
                    .posterData(bData)
                    .imageName(file.getOriginalFilename())
                    .build();
            publicationImages.save(publicationImage);*/
            var obj = Publication.builder()
                    .image(file.getOriginalFilename().toLowerCase())
                    .description(description)
                    .datetime(LocalDateTime.now().toString())
                    .user(users.getById(userId))
                    .commentsQty(0)
                    .build();
            publications.save(obj);
            //return "publication created Successfully";
        } catch (IndexOutOfBoundsException | IOException ex) {
            //return "Error!";
            System.out.println(ex);
        }
    }

    public boolean deletePublication(Authentication authentication, String publicationId) {
        boolean res = false;
        User user = (User)authentication.getPrincipal();
        if (user.getId().equals(publications.getById(publicationId).getUser().getId())){
            publications.deleteById(publicationId);
            if (comments.existsAllByPublicationId(publicationId))
                comments.deleteAllByPublicationId(publicationId);
            res = true;
        }
        return res;
    }
    public List<Publication> getPublicationsByPublisherId(String id){
        return publications.findAllByUserId(id);
    }
/*
    public List<Publication> getPublicationsByPublisherLogin(String login){
        publications.findAllByUserLogin(login).stream().forEach(System.out::println);
        return publications.findAllByUserLogin(login);
    }
*//*
    public List<Publication> getPublicationsBySubscribes2(String id){
        List<Publication> publicationList = new ArrayList<>();
        subscribeService.getAllByUserSubscriberId(id).stream()
                .forEach(subscribe -> publicationList.addAll(publications.findAllByUserId(subscribe.getUserSubscribedTo().getId())));

        return publicationList;
    }
*//*
    public List<PublicationDTO> getPublicationsBySubscribes(String id){
        List<PublicationDTO> pubList = new ArrayList<>();
        subscribeService.getAllByUserSubscriberId(id).stream()
                .forEach(subscribe -> {
                            var publ = publications.findAllByUserId(subscribe.getUserSubscribedTo().getId());
                            pubList.addAll(PublicationDTO.fromList(publ));
                        });
        return pubList;
    }*/
}
