package kg.microgram.microgram.service;

import kg.microgram.microgram.dto.PublicationDTO;
import kg.microgram.microgram.dto.ResponceDTO;
import kg.microgram.microgram.dto.SubscribeDTO;
import kg.microgram.microgram.dto.UserDTO;
import kg.microgram.microgram.exception.ResourceNotFoundException;
import kg.microgram.microgram.model.Publication;
import kg.microgram.microgram.model.PublicationImage;
import kg.microgram.microgram.model.Subscribe;
import kg.microgram.microgram.model.User;
import kg.microgram.microgram.repository.Subscribes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class SubscribeService {
    @Autowired
    Subscribes subscribes;
    public Slice<SubscribeDTO> findSubscribes(Pageable pageable) {
        var slice = subscribes.findAll(pageable);
        return slice.map(SubscribeDTO::from);
    }

    public SubscribeDTO findOne(String id) {
        var obj = subscribes.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Can't find subscribe with the ID: " + id));
        return SubscribeDTO.from(obj);
    }
    public ResponceDTO addSubscribe(SubscribeDTO subscribeData, Authentication authentication) {
        try{
            User user = (User)authentication.getPrincipal();
            var obj = Subscribe.builder()
                    .userSubscriber(user)
                    .userSubscribedTo(subscribeData.getUserSubscribedTo())
                    .build();
            subscribes.save(obj);
            return ResponceDTO.from(obj.getId(), "Subscribe " + obj.getId() + " created Successfully");
        } catch (java.lang.IndexOutOfBoundsException ex){
            return ResponceDTO.from("400", "Error!");
        }
    }

    public boolean deleteSubscribe(String id) {
        subscribes.deleteById(id);
        return true;
    }


    /*
    //найдет все подписки по Id, подпищиком которых является пользователь
    public List<SubscribeDTO> getAllByUserSubscriberId(String id){
        var subs = subscribes.getAllByUserSubscriberId(id);
        return SubscribeDTO.fromList(subs);
    }
    */

}
