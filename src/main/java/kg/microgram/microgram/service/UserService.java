package kg.microgram.microgram.service;

import kg.microgram.microgram.dto.NewUserDTO;
import kg.microgram.microgram.dto.PublicationDTO;
import kg.microgram.microgram.dto.ResponceDTO;
import kg.microgram.microgram.dto.UserDTO;
import kg.microgram.microgram.exception.ResourceNotFoundException;
import kg.microgram.microgram.model.Publication;
import kg.microgram.microgram.model.User;
import kg.microgram.microgram.repository.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class UserService {
    @Autowired
        Users users;

    public Slice<UserDTO> findUsers(Pageable pageable) {
        var slice = users.findAll(pageable);
        return slice.map(UserDTO::from);
    }

    public UserDTO findOne(String userId) {
        var user = users.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("Can't find user with the ID: " + userId));
        return UserDTO.from(user);
    }
    public UserDTO findOneByName(String name) {
        var user = users.findByName(name);
        return UserDTO.from(user);
    }
    public UserDTO findOneByLogin(String login) {
        var user = users.findByLogin(login);
        return UserDTO.from(user);
    }
    public UserDTO findOneByEmail(String email) {
        var user = users.findByEmail(email);
        return UserDTO.from(user);
    }
    public ResponceDTO addUser(NewUserDTO userData) {
        try{
            boolean res = users.existsByEmail(userData.getEmail());
            if (res) {
                return ResponceDTO.from("400", "Error! User already exists");
                //System.out.println("User already exists");
            } else {
                var user = User.builder()
                        .email(userData.getEmail())
                        .login(userData.getLogin())
                        .password(new BCryptPasswordEncoder().encode(userData.getPassword()))
                        .name(userData.getName())
                        .surname(userData.getSurname())
                        .publicationQty(0)
                        .subscriptionQty(0)
                        .subscriberQty(0)
                        .build();
                users.save(user);
                return ResponceDTO.from(user.getId(), "User " + user.getId() + " created Successfully");
                //System.out.println("Error");
            }
        } catch (java.lang.IndexOutOfBoundsException ex){
            return ResponceDTO.from("400", "Error!");
            //System.out.println("Error");
        }
    }
    public ResponceDTO addUser2(String email, String login, String password, String name, String surname) {
        try{
            boolean res = users.existsByEmail(email);
            if (res) {
                return ResponceDTO.from("400", "Error! User already exists");
                //System.out.println("User already exists");
            } else {
                var user = User.builder()
                        .email(email)
                        .login(login)
                        .password(new BCryptPasswordEncoder().encode(password))
                        .name(name)
                        .surname(surname)
                        .publicationQty(0)
                        .subscriptionQty(0)
                        .subscriberQty(0)
                        .build();
                users.save(user);
                return ResponceDTO.from(user.getId(), "User " + user.getId() + " created Successfully");
                //System.out.println("Success");
            }
        } catch (java.lang.IndexOutOfBoundsException ex){
            return ResponceDTO.from("400", "Error!");
            //System.out.println("Error");
        }
    }
}
