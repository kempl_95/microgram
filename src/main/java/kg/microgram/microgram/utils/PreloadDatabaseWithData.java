package kg.microgram.microgram.utils;

import kg.microgram.microgram.model.*;
import kg.microgram.microgram.repository.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

@Configuration
public class PreloadDatabaseWithData {
    private static final Random rn = new Random();
    @Bean
    CommandLineRunner fillDatabase(Users users, Publications publications, PublicationImages publicationImages, Comments comments, Likes likes, Subscribes subscribes){
        String[] userNames = {"Kamil", "Ulia", "Milen", "Amal", "Nael"};
        String[] userSurnames = {"Mirzhalalov", "Potapova", "Mirzhalalov", "Arapbaev", "Vaitkus"};
        String[] userLogins = {"kempl95kg", "potapovaUlia", "antAras", "arapbaev88", "ntw98"};
        AtomicInteger cnt = new AtomicInteger();


        return (args) -> {
            users.deleteAll();
            publications.deleteAll();
            publicationImages.deleteAll();
            comments.deleteAll();
            likes.deleteAll();
            subscribes.deleteAll();


            //Добавляю пользователей
            List<User> userList = new ArrayList<>();
            for (int i = 0; i < userNames.length; i++){
                userList.add(User.addBegUsers(String.valueOf(i+1), userNames[i], userSurnames[i], userLogins[i]));
            }
            users.saveAll(userList);
            //ДОБАВЬ В БАЗУ NO-IMAGE
            //publicationImages.save(PublicationImage.NO_IMAGE);
            //Добавляю публикации
            List<Publication> publicationList = new ArrayList<>();
            cnt.set(0);
            for (User user: userList){
                for (int i = 1; i<=user.getPublicationQty(); i++){
                    cnt.set(cnt.get() + 1);
                    publicationList.add(Publication.addBegPublications(String.valueOf(cnt.get()), user));
                }
            }
            publications.saveAll(publicationList);
            //Добавляю комментарии
            List<Comment> commentList = new ArrayList<>();
            cnt.set(0);
            for (Publication publication: publicationList){
                for (int i = 1; i<=publication.getCommentsQty(); i++){
                    cnt.set(cnt.get() + 1);
                    commentList.add(Comment.addBegComments(String.valueOf(cnt.get()), userList.get(rn.nextInt(userList.size())), publication));
                }
            }
            comments.saveAll(commentList);
            //Добавляю лайки
            List<Like> likeList = new ArrayList<>();
            cnt.set(0);
            for (Publication publication: publicationList){
                cnt.set(cnt.get() + 1);
                likeList.add(Like.addBegLikes(String.valueOf(cnt.get()), userList.get(rn.nextInt(userList.size())), publication));
            }
            likes.saveAll(likeList);
            //Добавляю подписки
            List<Subscribe> subscribeList = new ArrayList<>();
            cnt.set(0);
            for (User user: userList){
                for (User userSubscribers: userList){
                    if (!user.getId().equals(userSubscribers.getId())){
                        cnt.set(cnt.get() + 1);
                        subscribeList.add(Subscribe.addBegSubscribes(String.valueOf(cnt.get()), user, userSubscribers));
                    }
                }
            }
            subscribes.saveAll(subscribeList);

            System.out.println("=================== DONE ===================");
        };
    }
}
