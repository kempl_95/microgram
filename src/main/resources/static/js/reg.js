'use strict';
const baseUrl = 'http://localhost:8888';

/*const registrationForm = document.getElementById("registration-form");
registrationForm.addEventListener('submit',  async function() {
    let data = new FormData(registrationForm);
    const userJSON = JSON.stringify(Object.fromEntries(data));
    console.log(userJSON);
    const response = await fetch(baseUrl + '/users', {
            method: 'POST',
            //cache: 'no-cache',
            //mode : 'cors',
            /*headers: {
                'Content-Type': 'application/json'
            },*//*
            body: userJSON
    }).then(res => console.log(res));
    console.log("Response: " + response);
    *//*const responseData = await response.json();
    console.log(responseData);
    window.location.href = baseUrl;*//*
});*/
//как у Алмаза

const registrationForm = document.getElementById('registration-form');
registrationForm.addEventListener('submit', onRegisterHandler);
function onRegisterHandler(e) {
    e.preventDefault();
    const form = e.target;
    const data = new FormData(form);
    const userJSON = JSON.stringify(Object.fromEntries(data));
    createUser(userJSON).then(r => console.log("Данные отправлены: ")).catch(e => console.log("Ошибка: " + e));
}
async function createUser(userJSON) {
    const settings = {
        method: 'POST',
        //cache: 'no-cache',
        //mode : 'cors',
        headers: {
            'Content-Type': 'application/json'
        },
        body: userJSON
    };
    const response = await fetch(baseUrl + '/users', settings);
    const responseData = await response.json();
    console.log(responseData);
    window.location.href = baseUrl;
}
//});