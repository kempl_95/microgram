'use strict';
let commentIconClickedArray = [];
const baseUrl = 'http://localhost:8888';
//Создайте объект пользователя
function User(id, name, surname, login, email, password, isAuthorized) {
    this.id = id;
    this.name = {
        name: name,
        surname: surname
    };
    this.login = login;
    this.email = email;
    this.password = password;
    this.isAuthorized = isAuthorized;               //Как понять, авторизован он или нет
}
function loginUser(email, password) {
    this.email = email;
    this.password = password;
}
//Создаю объект лайк
function Like(id, user, datetime) {
    this.id = id;
    this.user = user;
    this.datetime = datetime;
}
//Создайте объект поста.
function Post(id, image, description, datetime, user, commentsList) {
    this.id = id;
    this.image = image;
    this.description = description;
    this.datetime = datetime;
    this.user = user;
    this.commentsList = commentsList;
}
//Создайте объект комментария
function Comment(id, text, datetime, user) {
    this.id = id;
    this.text = text;
    this.datetime = datetime;
    this.user = user;
}
function createCommentElement(comment) {
    let commentElement = document.createElement('div');
    commentElement.className = "py-2 pl-3";
    let userLink = document.createElement('a');
    let commentText = document.createElement('p');
    userLink.className = "muted";
    userLink.href = "/users/" + comment.user.id.toString();
    userLink.innerHTML = comment.user.login;
    commentText.innerHTML = comment.text;

    //Соединяю
    commentElement.append(userLink);
    userLink.after(commentText);
    return commentElement;
}

function addCommentElement(commentElement){
    document.getElementsByClassName('comments')[0].append(commentElement);
}
function addCommentElementToNewPost(commentElement, postElement){
    postElement.getElementsByClassName('comments')[0].append(commentElement);
}
function createPostElement(post, idx) {
    let postElement = document.createElement('div');
    postElement.className = "card my-3 post_block";
    postElement.innerHTML =
        '<!-- image block start -->' +
        '                    <div class="image">' +
        '                        <span class="h1 m-0 text-danger">' +
        '                           <i class="fas fa-heart"></i>' +
        '                        </span>' +
        `                        <img class="d-block w-100" src="/img/${post.image}">` +
        '                    </div>' +
        '                    <!-- image block end -->' +
        '                    <div class="px-4 py-3">' +
        '\n' +
        '                        <!-- post reactions block start -->' +
        '                        <div class="d-flex justify-content-around">\n' +
        '                            <span class="h1 mx-2 text-danger heart">\n' +
        '                                <i class="fas fa-heart"></i>\n' +
        '                            </span>\n' +
        '                            <span class="h1 mx-2 muted heart">\n' +
        '                                <i class="far fa-heart"></i>\n' +
        '                            </span>\n' +
        '                            <span class="h1 mx-2 muted comment">\n' +
        '                                <i class="far fa-comment"></i>\n' +
        '                            </span>\n' +
        '                            <span class="mx-auto"></span>\n' +
        '                            <span class="h1 mx-2 muted unmarkedBookmark">\n' +
        '                                <i class="far fa-bookmark"></i>\n' +
        '                            </span>\n' +
        '                            <span class="h1 mx-2 muted markedBookmark">\n' +
        '                                <i class="fas fa-bookmark"></i>\n' +
        '                            </span>\n' +
        '                        </div>\n' +
        '                        <!-- post reactions block end -->\n' +
        '                        <hr>\n' +
        '                        <!-- post section start -->\n' +
        '                        <div>\n' +
        `                            <p class="post-description">${post.description}</p>\n` +
        '                        </div>\n' +
        '                        <!-- post section end -->\n' +
        '                        <hr>\n' +
        '                        <!-- comments section start -->\n' +
        '                        <div class="comments">' +
        '                        </div>' +
        '                        <!-- comments section end -->\n' +
        '                           <form method="post" enctype="multipart/form-data" action="/comments" class="comment-form">' +
        '                              <hr>' +
        '                              <div class="example-2">' +
        '                                  <div class="form-group">' +
        '                                        <div class="input-group input-group-default mb-3">' +
        '                                             <input type="text" name="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-lg" placeholder="Добавьте комментарий...">' +
        '                                             <input type="hidden" name="userId" value="1">' +
        `                                             <input type="hidden" name="publicationId" value="${post.id}">` +
        '                                             <button class="btn btn-secondary btn-default" type="submit" name="add-comment">Опубликовать</button>' +
        '                                       </div>' +
        '                                  </div>' +
        '                           </form>' +
        '                        </div>'+
        '                    </div>'
    ;
    for (let i = 0; i<post.commentsList.length; i++){
        addCommentElementToNewPost(createCommentElement(post.commentsList[i]), postElement);
    }
    commentIconClickedArray.push(false);
    postElement.getElementsByClassName("comment-form")[0].style.display = "none";

    let commentIcon = postElement.getElementsByClassName("h1 mx-2 muted comment")[0];
    commentIcon.addEventListener('click', function () {
        if (commentIconClickedArray[idx]){
            postElement.getElementsByClassName("comment-form")[0].style.display = "none";
            commentIconClickedArray[idx] = false;
        } else if (!commentIconClickedArray[idx]){
            postElement.getElementsByClassName("comment-form")[0].style.display = "inline-block";
            postElement.getElementsByClassName("comment-form")[0].style.width = "100%";
            commentIconClickedArray[idx] = true;
        }
    });
    return postElement;
}
function addPost(postElement) {
    document.getElementsByClassName("col posts-container")[0].append(postElement);
}
function showSplashScreen() {
    document.getElementById("page-splash").style.display = "flex";
    document.body.className = "no-scroll";
}
function hideSplashScreen() {
    document.getElementById("page-splash").style.display = 'none';
    document.body.className = "";
}
//Другой подход. Id использовать неправильно. на странице постов может быть много. Поэтому надо использовать цикл изначально. Смотреть по className

function publicationsGetAll() {
    const posts = document.getElementById("posts-container");
    //let postsData;
    //let commentsData;
    if (posts !== undefined) {
        fetch('http://localhost:8888/publications/all').then(res => {
            if (res.ok) {
                res.json()
                    .then(data => {
                        for (let i = 0; i<data.length; i++){
                            let comments = [];
                            for (let k = 0; k<data[i].commentsList.length; k++){
                                comments.push(new Comment(data[i].commentsList[k].id, data[i].commentsList[k].text, data[i].commentsList[k].datetime, data[i].commentsList[k].user));
                            }
                            addPost(createPostElement(new Post(data[i].id ,data[i].image, data[i].description, data[i].datetime, data[i].user, comments), i));
                            uploadAnimation();
                        }
                    }).catch(err => {
                    console.log("I am mistake: " + err);
                });
            } else {
                console.log(res.status)
            }
        });

    }
}
const postForm = document.getElementById("post-form");
postForm.addEventListener('submit', function () {
    let data = new FormData(postForm);
    fetch('http://localhost:8888/publications/add', {
        method: 'POST',
        body: data
    }).then(r => {
        console.log(r.status);
        /*r.redirect('http://localhost:8888/').then(res => {
            console.log("Redirected");
            if (res.ok) {
                res.json()
                    .then(data => {
                        console.log(data.length);
                        for (let i = 0; i<data.length; i++){
                            //console.log(data[i].image +" || "+ data[i].description +" || "+ data[i].user);
                            addPost(createPostElement(new Post(data[i].id ,data[i].image, data[i].description, data[i].datetime, data[i].user)));
                            uploadAnimation();
                        }
                    }).catch(err => {
                    console.log("I am mistake: " + err);
                });
            } else {
                console.log(res.status)
            }*/
        }).catch(err => console.log(err));
    });
//});
for (let i = 0; i<document.getElementsByClassName("comment-form").length; i++){
    let commentForm = document.getElementsByClassName("comment-form")[i];
    commentForm.addEventListener('submit', function () {
        let data = new FormData(commentForm);
        fetch('http://localhost:8888/comments/add', {
            method: 'POST',
            body: data
        }).then(r => {
            console.log(r.status);
        }).catch(err => console.log(err));
    })
}
function saveUser(user) {
    const userAsJSON = JSON.stringify(user)
    localStorage.setItem('user', userAsJSON);
}
function restoreUser() {
    const userAsJSON = localStorage.getItem('user');
    return JSON.parse(userAsJSON);
}
//------------------------------ Логин/Вход ------------------------------
const loginForm = document.getElementById('login-form');
loginForm.addEventListener('submit', onLoginHandler);
async function onLoginHandler(e) {
    e.preventDefault();
    const form = e.target;
    const userFormData = new FormData(form);
    const user = Object.fromEntries(userFormData);
    saveUser(user);
    updateRootPage();
}
function updateRootPage() {
    console.log("LS USER: " + localStorage.getItem('user'));
    if (localStorage.getItem('user') == null){
        showSplashScreen();
    } else {
        fetchAuthorised(baseUrl+'/publications').then(res => {
            if (res.ok) {
                res.json()
                    .then(data => {
                        for (let i = 0; i<data.length; i++){
                            let comments = [];
                            for (let k = 0; k<data[i].commentsList.length; k++){
                                comments.push(new Comment(data[i].commentsList[k].id, data[i].commentsList[k].text, data[i].commentsList[k].datetime, data[i].commentsList[k].user));
                            }
                            addPost(createPostElement(new Post(data[i].id ,data[i].image, data[i].description, data[i].datetime, data[i].user, comments), i));
                            uploadAnimation();
                        }
                    }).catch(err => {
                    console.log("I am mistake: " + err);
                });
            } else {
                console.log(res.status)
            }
        });
        hideSplashScreen();
    }
}

function updateOptions(options) {
    const update = { ...options };
    update.mode = 'cors';
    update.headers = { ... options.headers };
    update.headers['Content-Type'] = 'application/json';
    const user = restoreUser();
    if(user) {
        update.headers['Authorization'] = 'Basic ' + btoa(user.login + ':' + user.password);
    }
    return update;
}
async function fetchAuthorised(url, options) {
    const settings = options || {};
    return await fetch(url, updateOptions(settings));
}
//------------------------------ Логин/Выход ------------------------------
const logOut = document.getElementById("log_out");
logOut.addEventListener('click', clearLocalStorage);
function clearLocalStorage(){
    localStorage.clear();
    updateRootPage();
}
//-- Тест ссылки на профиль --
function writeProfileLinkLoginName() {
    const profileLink = document.getElementById("profile_link");
    const user = restoreUser();
    profileLink.innerText = user.login;
}
writeProfileLinkLoginName();
//================================================================================================================== ФУНКЦИЯ ДЛЯ НАЧАЛЬНОЙ СТРАНИЦЫ ====================================================
updateRootPage();
//================================================================================================================== ФУНКЦИЯ ДЛЯ НАЧАЛЬНОЙ СТРАНИЦЫ ====================================================



console.log("отработал main");
//Анимацию в конце

function uploadAnimation() {
    //console.log("size: " + document.getElementsByClassName("h1 mx-2 muted heart").length);
    for (let i = 0; i<document.getElementsByClassName("h1 mx-2 muted heart").length;i++){
        let unmarkedHeart =  document.getElementsByClassName("h1 mx-2 muted heart")[i];
        let markedHeart =  document.getElementsByClassName("h1 mx-2 text-danger heart")[i];
        markedHeart.style.display = "none";                                         //К каждому unmarkedHeart прячу markHeart
        unmarkedHeart.addEventListener('click', function () {
            unmarkedHeart.style.display = 'none';
            markedHeart.style.display = 'inline-block';
        });
        markedHeart.addEventListener('click', function () {
            unmarkedHeart.style.display = 'inline-block';
            markedHeart.style.display = 'none';
        });
    }

    for (let i = 0; i<document.getElementsByClassName("image").length;i++){
        let imgDiv = document.getElementsByClassName("image")[i];
        let img = imgDiv.getElementsByClassName("d-block w-100")[0];
        img.addEventListener('dblclick', function () {
            let like = imgDiv.getElementsByClassName("h1 m-0 text-danger")[0];
            like.style.transform = "scale(7) scale(0)";
        });
    }
    for (let i = 0; i<document.getElementsByClassName("h1 mx-2 muted unmarkedBookmark").length;i++) {
        let unmarkedBookmark = document.getElementsByClassName("h1 mx-2 muted unmarkedBookmark")[i];
        let markedBookmark = document.getElementsByClassName("h1 mx-2 muted markedBookmark")[i];
        markedBookmark.style.display = 'none';
        unmarkedBookmark.addEventListener('click', function () {
            unmarkedBookmark.style.display = 'none';
            markedBookmark.style.display = 'inline-block';
        });
        markedBookmark.addEventListener('click', function () {
            unmarkedBookmark.style.display = 'inline-block';
            markedBookmark.style.display = 'none';
        });
    }

    (function() {
        $('.input-file').each(function() {
            var $input = $(this),
                $label = $input.next('.js-labelFile'),
                labelVal = $label.html();
            $input.on('change', function(element) {
                var fileName = '';
                if (element.target.value) fileName = element.target.value.split('\\').pop();
                fileName ? $label.addClass('has-file').find('.js-fileName').html(fileName) : $label.removeClass('has-file').html(labelVal);
            });
        });
    })();
    //console.log("отработал animation");
}
uploadAnimation();
//Анимацию в конце